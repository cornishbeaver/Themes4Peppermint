#!/bin/sh
sed -i \
         -e 's/#3b3e3f/rgb(0%,0%,0%)/g' \
         -e 's/#e5e5e5/rgb(100%,100%,100%)/g' \
    -e 's/#3b3e3f/rgb(50%,0%,0%)/g' \
     -e 's/#285bff/rgb(0%,50%,0%)/g' \
     -e 's/#3b3e3f/rgb(50%,0%,50%)/g' \
     -e 's/#d3dae3/rgb(0%,0%,50%)/g' \
	"$@"
