# Themes4Peppermint

themes I have created that will hopefully be of use to the peppermintOS community.

## included here are:

* theme for XFCE4
* theme for GTK2 and GTK3
* Openbox theme
* cinnamon theme
* Icons
* Config for Cinnamon desktop
There are also matching Icon themes for both red and blue.
Just added- badger grey themes. which are as they are called a grey colour with a hint of red.
And there are a set of configs which will help set up the cinammon desktop

note: in order to apply these configs one needs to install  " SaveDesktop" app. which can be donwloaded using flatpak.
> flatpak install savedesktop 

this app will automatically apply all the configs. Alternatively you could download the themes and Icon files and just use them, if you wish.